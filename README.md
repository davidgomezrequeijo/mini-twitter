WebTwitter 

## Production enviroment

[WebTwitter](https://twitterweb-production.herokuapp.com/)

## Register

You can register with your name and your email

[Register](https://twitterweb-production.herokuapp.com/register)

![Register](resources/Register.png)

## Login

If you have a registered user, you can make login:

[Login](https://twitterweb-production.herokuapp.com/login)

![Login](resources/Login.png)

## Home

After making Login, you should see the application empty. You can post a message, search users to follow or see the users you are following

![Home](resources/Home.png)

# Development

## How to execute the app in dev mode

Go to server folder and install dependencies:

```
cd server
yarn
```

Go to root folder and install client dependencies

```
yarn
```

Execute app:

```
yarn start:dev
```

## How to execute tests

```
yarn test
```

## Continous Integration

There is a pipeline configured to execute when every merge request is merged with:

- Build
- Eslint
- Test

When one branch is merged to master another step is executed:

- Deploy to Heroku
