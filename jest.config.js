module.exports = {
    verbose: true,
    testMatch: ['**/__tests__/**/?(*.)+(spec|test).[jt]s?(x)'],
    transform: { '^.+\\.(ts|tsx)?$': 'ts-jest' },
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
    testEnvironment: 'jsdom',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    rootDir: './src',
    coveragePathIgnorePatterns: ['__tests__'],
};
