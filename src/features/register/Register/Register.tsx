import React, { ReactElement, useState } from 'react';
import styled from 'styled-components';
import { User } from '../../../base/types';
import { StyledInput } from '../../../base/components/inputText/inputText';
import { StyledInputButton } from '../../../base/components/inputButton';
import { NavLink } from 'react-router-dom';

const StyledContainerForm = styled.div`
    position: absolute;
    top: 30%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

const StyledTitle = styled.h1`
    color: #ffffff;
    font-family: 'Roboto';
`;

const StyledInputRegister = styled(StyledInput)`
    margin: 10px 0px;
`;

const StyledNavLink = styled(NavLink)`
    color: #007ec3;
    margin-left: 10px;
`;

interface RegisterProps {
    onSubmit: (user: User) => void;
}

const Register = ({ onSubmit }: RegisterProps): ReactElement => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');

    const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    };

    const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value);
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        onSubmit({ id: new Date().getTime(), name, email });
    };

    return (
        <StyledContainerForm>
            <StyledTitle>Register in Web Twitter</StyledTitle>
            <form onSubmit={handleSubmit}>
                <StyledInputRegister type="text" placeholder="Name" onChange={handleChangeName} />
                <StyledInputRegister type="text" placeholder="Email" onChange={handleChangeEmail} />
                <StyledInputButton type="submit" value="Register" />
                <StyledNavLink to="/login">Login</StyledNavLink>
            </form>
        </StyledContainerForm>
    );
};

export default Register;
