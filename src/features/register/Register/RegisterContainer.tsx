import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState, User } from '../../../base/types';
import { registerUser } from '../registerReducer';
import Register from './Register';

const RegisterContainer = (): ReactElement => {
    const dispatch = useDispatch();
    const history = useHistory();
    const { user } = useSelector((state: RootState) => state.auth);

    const handleSubmit = (user: User) => {
        event.preventDefault();
        dispatch(registerUser(user));
    };

    useEffect(() => {
        if (user !== null) {
            history.push('/');
        }
    }, [user]);

    return <Register onSubmit={handleSubmit} />;
};

export default RegisterContainer;
