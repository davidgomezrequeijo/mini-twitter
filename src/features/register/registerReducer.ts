import { Action } from '../../base/types/redux';
import ReducerRegistry from '../../base/redux/reducers/ReducerRegistry';
import { User } from '../../base/types';

export interface RegisterState {
    loading: boolean;
}

export const initialState: RegisterState = {
    loading: false,
};

const registerReducer = (state: RegisterState = initialState, action: Action<void>): RegisterState => {
    switch (action.type) {
        case REGISTER_USER:
            return {
                ...state,
                loading: true,
            };
        default:
            return state;
    }
};

export const REGISTER_USER = 'register/registerUser';

export const registerUser = (payload: User): Action<User> => ({
    type: REGISTER_USER,
    payload,
});

ReducerRegistry.register('register', registerReducer);

export default registerReducer;
