import { Middleware } from 'redux';
import axios, { AxiosResponse } from 'axios';
import { Action, Handlers } from '../../base/types/redux';
import MiddlewareRegistry from '../../base/redux/middlewares/MiddlewareRegistry';
import { REGISTER_USER } from './registerReducer';
import { User } from '../../base/types';
import { setUser } from '../auth/authReducer';
import { BROWSER_STORAGE_AUTH_KEY } from '../auth/authMiddleware';

const registerMiddleware: Middleware = (middlewareAPI) => (next) => (action: Action) => {
    next(action);
    const handler = handlers[action.type];
    if (handler) {
        handler(middlewareAPI, action);
    }
};

const handlers: Handlers<void> = {
    [REGISTER_USER]: async ({ dispatch }, action) => {
        axios.post('/api/user/register', { user: action.payload }).then((response: AxiosResponse<User>) => {
            const { id, name, email } = response.data;
            const user: User = {
                id,
                name,
                email,
            };
            dispatch(setUser(user));
            localStorage.setItem(BROWSER_STORAGE_AUTH_KEY, JSON.stringify(user));
        });
    },
};

MiddlewareRegistry.register(registerMiddleware);

export default registerMiddleware;
