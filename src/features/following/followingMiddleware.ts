import { Middleware } from 'redux';
import axios, { AxiosResponse } from 'axios';
import { Action, Handlers } from '../../base/types/redux';
import { GET_FOLLOWING_USERS, setFollowingUsers, FOLLOW_USER, UNFOLLOW_USER } from './followingReducer';
import MiddlewareRegistry from '../../base/redux/middlewares/MiddlewareRegistry';
import { User } from '../../base/types';

const followingMiddleware: Middleware = (middlewareAPI) => (next) => (action: Action) => {
    next(action);
    const handler = handlers[action.type];
    if (handler) {
        handler(middlewareAPI, action);
    }
};

const handlers: Handlers<void> = {
    [GET_FOLLOWING_USERS]: async ({ dispatch }, action) => {
        axios.get(`/api/user/following?id=${action.payload}`).then((response: AxiosResponse<User[]>) => {
            dispatch(setFollowingUsers(response.data));
        });
    },
    [FOLLOW_USER]: async ({ getState, dispatch }, action) => {
        const currentUser = getState().auth.user;
        axios
            .post('/api/user/follow', { currentUser, followUser: action.payload })
            .then((response: AxiosResponse<User[]>) => {
                dispatch(setFollowingUsers(response.data));
            });
    },
    [UNFOLLOW_USER]: async ({ getState, dispatch }, action) => {
        const currentUser = getState().auth.user;
        axios
            .post('/api/user/unfollow', { currentUser, unfollowUser: action.payload })
            .then((response: AxiosResponse<User[]>) => {
                dispatch(setFollowingUsers(response.data));
            });
    },
};

MiddlewareRegistry.register(followingMiddleware);

export default followingMiddleware;
