import React, { ReactElement, useState } from 'react';
import { User } from '../../../../base/types';
import UserInfo from '../../../../base/components/userInfo';
import { StyledButton } from '../../../../base/components/button';
import styled from 'styled-components';

interface FollowingProps {
    user: User;
    onClickUnfollow: (user: User) => void;
}

const StyledListItem = styled.li`
    display: flex;
    justify-content: space-between;
    margin: 10px 0px;
`;

const FollowingItem = ({ user, onClickUnfollow }: FollowingProps): ReactElement => {
    const [isVisibleFollowButton, setIsVisibleFollowButton] = useState(false);

    const handleMouseEnter = () => {
        setIsVisibleFollowButton(true);
    };

    const handleMouseLeave = () => {
        setIsVisibleFollowButton(false);
    };
    return (
        <StyledListItem key={user.id} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
            <UserInfo user={user} />
            {isVisibleFollowButton && <StyledButton onClick={() => onClickUnfollow(user)}>Unfollow</StyledButton>}
        </StyledListItem>
    );
};

export default FollowingItem;
