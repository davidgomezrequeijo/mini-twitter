import React, { ReactElement } from 'react';
import { User } from '../../../../base/types';
import { StyledTitle } from '../../../../base/components/titleWidget';
import { StyledList } from '../../../../base/components/list';
import FollowingItem from './FollowingItem';

interface FollowingProps {
    users: User[];
    onClickUnfollow: (user: User) => void;
}

const Following = ({ users, onClickUnfollow }: FollowingProps): ReactElement => {
    return (
        <>
            <StyledTitle>Following</StyledTitle>
            <StyledList>
                {users.map((user) => (
                    <FollowingItem key={user.id} user={user} onClickUnfollow={onClickUnfollow} />
                ))}
            </StyledList>
        </>
    );
};

export default Following;
