import React, { ReactElement, useEffect } from 'react';
import { getFollowingUsers, unfollowUser } from '../../followingReducer';
import { useDispatch, useSelector } from 'react-redux';
import Following from './Following';
import { RootState, User } from '../../../../base/types';
import Loading from '../../../../base/components/loading';

interface FollowingContainerProps {
    idUser: number;
}

const FollowingContainer = ({ idUser }: FollowingContainerProps): ReactElement => {
    const dispatch = useDispatch();
    const { followingUsers, loading } = useSelector((state: RootState) => state.following);

    useEffect(() => {
        dispatch(getFollowingUsers(idUser));
    }, []);

    const handleUnfollow = (user: User) => {
        dispatch(unfollowUser(user));
    };

    if (loading) {
        return <Loading />;
    }

    return <Following users={followingUsers} onClickUnfollow={handleUnfollow} />;
};

export default FollowingContainer;
