import React, { ReactElement } from 'react';
import { User } from '../../../../base/types';
import { StyledTitle } from '../../../../base/components/titleWidget';
import { StyledList } from '../../../../base/components/list';
import UserInfo from '../../../../base/components/userInfo';

interface FollowingProps {
    users: User[];
}

const Following = ({ users }: FollowingProps): ReactElement => {
    return (
        <>
            <StyledTitle>Following</StyledTitle>
            <StyledList>
                {users.map((user) => (
                    <li key={user.id}>
                        <UserInfo user={user} />
                    </li>
                ))}
            </StyledList>
        </>
    );
};

export default Following;
