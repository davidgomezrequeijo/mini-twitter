import { User } from '../../base/types';
import { Action } from '../../base/types/redux';
import ReducerRegistry from '../../base/redux/reducers/ReducerRegistry';

export interface FollowingState {
    loading: boolean;
    followingUsers: User[];
}

export const initialState: FollowingState = {
    loading: true,
    followingUsers: [],
};

const followingReducer = (
    state: FollowingState = initialState,
    action: Action<void | User[] | number>,
): FollowingState => {
    switch (action.type) {
        case GET_FOLLOWING_USERS: {
            return {
                ...state,
                loading: true,
            };
        }
        case SET_FOLLOWING_USERS: {
            return {
                ...state,
                loading: false,
                followingUsers: action.payload as User[],
            };
        }
        default:
            return state;
    }
};

export const GET_FOLLOWING_USERS = 'following/getFollowingUsers';
export const SET_FOLLOWING_USERS = 'following/setFollowingUsers';
export const FOLLOW_USER = 'following/followUser';
export const UNFOLLOW_USER = 'following/unfollowUser';

export const getFollowingUsers = (payload: number): Action<number> => ({
    type: GET_FOLLOWING_USERS,
    payload,
});

export const setFollowingUsers = (payload: User[]): Action<User[]> => ({
    type: SET_FOLLOWING_USERS,
    payload,
});

export const followUser = (payload: User): Action<User> => ({
    type: FOLLOW_USER,
    payload,
});

export const unfollowUser = (payload: User): Action<User> => ({
    type: UNFOLLOW_USER,
    payload,
});

ReducerRegistry.register('following', followingReducer);

export default followingReducer;
