import { AnyAction, Dispatch } from 'redux';
import { getFollowingUsers, setFollowingUsers } from '../followingReducer';
import followingMiddleware from '../followingMiddleware';
import axios from 'axios';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

let dispatch: Dispatch<AnyAction>;
let getState: any;
let next: Dispatch<AnyAction>;

const setup = () => {
    dispatch = jest.fn();
    getState = jest.fn();
    getState.mockReturnValue({});
    next = jest.fn();
    jest.clearAllMocks();
};

describe('Following middleware', () => {
    beforeEach(() => {
        setup();
    });
    it('should get following user from API', () => {
        const userId = 123456;
        const action = getFollowingUsers(123456);

        followingMiddleware({ dispatch, getState })(next)(action);

        expect(axios.get).toHaveBeenCalledWith(`/api/user/following?id=${userId}`);
    });
    it('should dispatch setFollowingUsers when getFollowingUsers action is dispatched', async () => {
        const action = getFollowingUsers(123456);

        const mockUsers = [
            {
                id: 1,
                name: 'Alice',
                email: 'alice@test.com',
            },
            {
                id: 2,
                name: 'Bob',
                email: 'bob@test.com',
            },
            {
                id: 3,
                name: 'Charlie',
                email: 'charlie@test.com',
            },
        ];
        const resp = { data: mockUsers };
        mockedAxios.get.mockResolvedValue(resp);

        await followingMiddleware({ dispatch, getState })(next)(action);

        expect(dispatch).toHaveBeenCalledWith(setFollowingUsers(mockUsers));
    });
});
