import followingReducer, { initialState, setFollowingUsers, getFollowingUsers } from '../followingReducer';

describe('Following reducer', () => {
    let getState;
    const setup = () => {
        getState = jest.fn();
        getState.mockReturnValue({});
    };
    beforeEach(() => {
        setup();
    });
    it('It should set loading true, when dispatching getFollowingUsers', () => {
        const action = getFollowingUsers(123456);

        expect(followingReducer(initialState, action)).toEqual({
            ...initialState,
            loading: true,
        });
    });
    it('It should add users to state and set loading false, when dispatching setFollowingUsers', () => {
        const newFollowingUsers = [
            {
                id: 1,
                name: 'Alice',
                email: 'alice@test.com',
            },
            {
                id: 2,
                name: 'Bob',
                email: 'bob@test.com',
            },
        ];
        const action = setFollowingUsers(newFollowingUsers);

        expect(followingReducer(initialState, action)).toEqual({
            ...initialState,
            loading: false,
            followingUsers: newFollowingUsers,
        });
    });
});
