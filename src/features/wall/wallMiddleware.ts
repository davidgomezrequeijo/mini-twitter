import { Middleware } from 'redux';
import axios, { AxiosResponse } from 'axios';
import { Action, Handlers } from '../../base/types/redux';
import { setPosts, GET_POSTS_WALL, getPostsWall, GET_POSTS_USER } from './wallReducer';
import MiddlewareRegistry from '../../base/redux/middlewares/MiddlewareRegistry';
import { Post } from '../../base/types';
import { MESSAGE_SENT } from '../share/shareReducer';

const wallMiddleware: Middleware = (middlewareAPI) => (next) => (action: Action) => {
    next(action);
    const handler = handlers[action.type];
    if (handler) {
        handler(middlewareAPI, action);
    }
};

const handlers: Handlers<void> = {
    [GET_POSTS_WALL]: async ({ getState, dispatch }) => {
        const currentUserId = getState().auth.user.id;
        axios.get(`/api/user/allposts/${currentUserId}`).then((response: AxiosResponse<Post[]>) => {
            dispatch(setPosts(response.data));
        });
    },
    [GET_POSTS_USER]: async ({ dispatch }, action) => {
        axios.get(`/api/user/posts/${action.payload}`).then((response: AxiosResponse<Post[]>) => {
            dispatch(setPosts(response.data));
        });
    },
    [MESSAGE_SENT]: ({ dispatch }) => {
        dispatch(getPostsWall());
    },
};

MiddlewareRegistry.register(wallMiddleware);

export default wallMiddleware;
