import { Action } from '../../base/types/redux';
import ReducerRegistry from '../../base/redux/reducers/ReducerRegistry';
import { Post } from '../../base/types';

export interface WallState {
    loading: boolean;
    posts: Post[];
}

export const initialState: WallState = {
    loading: true,
    posts: [],
};

const wallReducer = (state: WallState = initialState, action: Action<void | Post[]>): WallState => {
    switch (action.type) {
        case GET_POSTS_USER: {
            return {
                ...state,
                loading: true,
            };
        }
        case GET_POSTS_WALL: {
            return {
                ...state,
                loading: true,
            };
        }
        case SET_POSTS: {
            return {
                ...state,
                loading: false,
                posts: action.payload as Post[],
            };
        }
        default:
            return state;
    }
};

export const GET_POSTS_WALL = 'wall/getPostsWall';
export const GET_POSTS_USER = 'wall/getPostsUser';
export const SET_POSTS = 'wall/setFollowingUsers';

export const getPostsWall = (): Action => ({
    type: GET_POSTS_WALL,
});

export const getPostsUser = (payload: number): Action<number> => ({
    type: GET_POSTS_USER,
    payload,
});

export const setPosts = (payload: Post[]): Action<Post[]> => ({
    type: SET_POSTS,
    payload,
});

ReducerRegistry.register('wall', wallReducer);

export default wallReducer;
