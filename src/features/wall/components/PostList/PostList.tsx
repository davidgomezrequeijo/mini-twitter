import React, { ReactElement } from 'react';
import TimeAgo from 'timeago-react';
import { Post } from '../../../../base/types';
import styled from 'styled-components';
import { StyledList } from '../../../../base/components/list';
import UserInfo from '../../../../base/components/userInfo';

interface PostListProps {
    posts: Post[];
}

const StyledPostListItem = styled.li`
    padding: 15px;
    font-family: 'Roboto';
    border-bottom: 1px solid #808080;
`;

const StyledMessage = styled.p`
    margin: 10px;
    color: #ffffff;
`;

const StyledTimeAgo = styled(TimeAgo)`
    font-family: 'Roboto';
    color: #ffffff;
`;

const StyledPostHeader = styled.div`
    display: flex;
    justify-content: space-between;
`;

const PostList = ({ posts }: PostListProps): ReactElement => {
    return (
        <StyledList>
            {posts.map((post) => (
                <StyledPostListItem key={post.id}>
                    <StyledPostHeader>
                        <UserInfo user={post.user} />
                        <StyledTimeAgo datetime={post.createdAt} />
                    </StyledPostHeader>
                    <StyledMessage>{post.message}</StyledMessage>
                </StyledPostListItem>
            ))}
        </StyledList>
    );
};

export default PostList;
