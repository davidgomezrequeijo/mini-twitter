import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../../base/types';
import { getPostsUser } from '../../wallReducer';
import PostList from './PostList';
import Loading from '../../../../base/components/loading';

interface PostListUserContainer {
    idUser: number;
}

const PostListUserContainer = ({ idUser }: PostListUserContainer): ReactElement => {
    const dispatch = useDispatch();
    const { posts, loading } = useSelector((state: RootState) => state.wall);

    useEffect(() => {
        dispatch(getPostsUser(idUser));
    }, []);

    if (loading) {
        return <Loading />;
    }

    return <PostList posts={posts} />;
};

export default PostListUserContainer;
