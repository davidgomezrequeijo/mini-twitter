import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../../base/types';
import { getPostsWall } from '../../wallReducer';
import PostList from './PostList';
import Loading from '../../../../base/components/loading';

const PostListWallContainer = (): ReactElement => {
    const dispatch = useDispatch();
    const { posts, loading } = useSelector((state: RootState) => state.wall);

    useEffect(() => {
        dispatch(getPostsWall());
    }, []);

    if (loading) {
        return <Loading />;
    }

    return <PostList posts={posts} />;
};

export default PostListWallContainer;
