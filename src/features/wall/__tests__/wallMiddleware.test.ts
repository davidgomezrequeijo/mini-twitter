import { AnyAction, Dispatch } from 'redux';
import { getPostsWall, setPosts } from '../wallReducer';
import wallMiddleware from '../wallMiddleware';
import axios from 'axios';
import { Post } from '../../../base/types';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

let dispatch: Dispatch<AnyAction>;
let getState: any;
let next: Dispatch<AnyAction>;

const setup = () => {
    dispatch = jest.fn();
    getState = jest.fn();
    getState.mockReturnValue({});
    next = jest.fn();
    jest.clearAllMocks();
};

describe('Wall middleware', () => {
    beforeEach(() => {
        setup();
    });
    it('should get posts from API', () => {
        const action = getPostsWall();
        const userId = 123456;
        getState.mockReturnValue({
            auth: {
                user: {
                    id: userId,
                },
            },
        });

        wallMiddleware({ dispatch, getState })(next)(action);

        expect(axios.get).toHaveBeenCalledWith(`/api/user/allposts/${userId}`);
    });
    it('should dispatch setPosts when getPostsWall action is dispatched', async () => {
        const action = getPostsWall();
        const userId = 123456;
        getState.mockReturnValue({
            auth: {
                user: {
                    id: userId,
                },
            },
        });

        const mockPosts: Post[] = [
            {
                id: 1,
                message: 'Hi!',
                createdAt: new Date().toDateString(),
                user: {
                    id: 1,
                    name: 'Alice',
                    email: 'alice@test.com',
                },
            },
            {
                id: 2,
                message: 'Welcome!',
                createdAt: new Date().toDateString(),
                user: {
                    id: 2,
                    name: 'Bob',
                    email: 'bob@test.com',
                },
            },
        ];
        const resp = { data: mockPosts };
        mockedAxios.get.mockResolvedValue(resp);

        await wallMiddleware({ dispatch, getState })(next)(action);

        expect(dispatch).toHaveBeenCalledWith(setPosts(mockPosts));
    });
});
