import wallReducer, { getPostsWall, initialState, setPosts } from '../wallReducer';

describe('Wall reducer', () => {
    let getState;
    const setup = () => {
        getState = jest.fn();
        getState.mockReturnValue({});
    };
    beforeEach(() => {
        setup();
    });
    it('It should set loading true, when dispatching getPosts', () => {
        const action = getPostsWall();

        expect(wallReducer(initialState, action)).toEqual({
            ...initialState,
            loading: true,
        });
    });
    it('It should add posts to state and set loading false, when dispatching setPosts', () => {
        const newPosts = [
            {
                id: 1,
                message: 'Hi!',
                createdAt: new Date().toDateString(),
                user: {
                    id: 1,
                    name: 'Alice',
                    email: 'alice@test.com',
                },
            },
            {
                id: 2,
                message: 'Welcome!',
                createdAt: new Date().toDateString(),
                user: {
                    id: 2,
                    name: 'Bob',
                    email: 'bob@test.com',
                },
            },
        ];
        const action = setPosts(newPosts);

        expect(wallReducer(initialState, action)).toEqual({
            ...initialState,
            loading: false,
            posts: newPosts,
        });
    });
});
