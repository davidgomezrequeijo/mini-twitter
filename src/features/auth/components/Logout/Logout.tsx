import React, { ReactElement } from 'react';
import { StyledButton } from '../../../../base/components/button';

interface LogoutProps {
    onLogout: () => void;
}

const Logout = ({ onLogout }: LogoutProps): ReactElement => {
    return <StyledButton onClick={onLogout}>Logout</StyledButton>;
};

export default Logout;
