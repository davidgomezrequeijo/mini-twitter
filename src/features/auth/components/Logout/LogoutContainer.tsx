import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from '../../../../base/types';
import { logout } from '../../authReducer';
import Logout from './Logout';

const LogoutContainer = (): ReactElement => {
    const dispatch = useDispatch();
    const history = useHistory();
    const { user } = useSelector((state: RootState) => state.auth);

    const handleLogout = () => {
        dispatch(logout());
    };

    useEffect(() => {
        if (user === null) {
            history.push('/');
        }
    }, [user]);

    return <Logout onLogout={handleLogout} />;
};

export default LogoutContainer;
