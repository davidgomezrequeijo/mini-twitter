import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from '../../../../base/types';
import { login } from '../../authReducer';
import Login from './Login';

const LoginContainer = (): ReactElement => {
    const dispatch = useDispatch();
    const history = useHistory();
    const { user, error } = useSelector((state: RootState) => state.auth);

    const handleSubmit = (email: string) => {
        dispatch(login(email));
    };

    useEffect(() => {
        if (user !== null) {
            history.push('/');
        }
    }, [user]);

    return <Login error={error} onSubmit={handleSubmit} />;
};

export default LoginContainer;
