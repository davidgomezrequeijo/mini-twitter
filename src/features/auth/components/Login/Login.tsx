import React, { ReactElement, useState } from 'react';
import styled from 'styled-components';
import { StyledInput } from '../../../../base/components/inputText/inputText';
import { StyledInputButton } from '../../../../base/components/inputButton';
import { NavLink } from 'react-router-dom';

interface LoginProps {
    error: string;
    onSubmit: (email: string) => void;
}

const StyledContainerForm = styled.div`
    position: absolute;
    top: 30%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

const StyledTitle = styled.h1`
    color: #ffffff;
    font-family: 'Roboto';
`;

const StyledNavLink = styled(NavLink)`
    color: #007ec3;
    margin-left: 10px;
`;

const StyledError = styled.span`
    color: #d48585;
    font-family: 'Roboto';
`;

const Login = ({ error, onSubmit }: LoginProps): ReactElement => {
    const [email, setEmail] = useState('');

    const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value);
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (email.length > 0) {
            onSubmit(email);
        }
    };

    return (
        <StyledContainerForm>
            <StyledTitle>Login in Web Twitter</StyledTitle>
            <form onSubmit={handleSubmit}>
                <StyledInput type="text" placeholder="Email" onChange={handleChangeEmail} />
                <StyledInputButton type="submit" value="Login" />
                <StyledNavLink to="/register">Register</StyledNavLink>
            </form>
            {error.length > 0 && <StyledError>{error}</StyledError>}
        </StyledContainerForm>
    );
};

export default Login;
