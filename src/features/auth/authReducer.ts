import { Action } from '../../base/types/redux';
import ReducerRegistry from '../../base/redux/reducers/ReducerRegistry';
import { User } from '../../base/types';

export interface AuthState {
    loading: boolean;
    user: User;
    error: string;
}

export const initialState: AuthState = {
    loading: false,
    user: null,
    error: '',
};

const authReducer = (state: AuthState = initialState, action: Action<void | User | string>): AuthState => {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                loading: true,
            };
        case LOGOUT:
            return {
                ...state,
                user: null,
            };
        case SET_USER:
            return {
                ...state,
                user: action.payload as User,
                error: '',
            };
        case SET_ERROR:
            return {
                ...state,
                error: action.payload as string,
            };
        default:
            return state;
    }
};

export const LOGIN = 'auth/login';
export const LOGOUT = 'auth/logout';
export const SET_USER = 'auth/setUser';
export const SET_ERROR = 'auth/setError';
export const INIT_AUTH = 'auth/initAuth';

export const login = (payload: string): Action<string> => ({
    type: LOGIN,
    payload,
});

export const logout = (): Action => ({
    type: LOGOUT,
});

export const setUser = (payload: User): Action<User> => ({
    type: SET_USER,
    payload,
});

export const initAuth = (): Action => ({
    type: INIT_AUTH,
});

export const setError = (payload: string): Action<string> => ({
    type: SET_ERROR,
    payload,
});

ReducerRegistry.register('auth', authReducer);

export default authReducer;
