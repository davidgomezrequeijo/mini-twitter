import { Middleware } from 'redux';
import axios, { AxiosResponse } from 'axios';
import { Action, Handlers } from '../../base/types/redux';
import MiddlewareRegistry from '../../base/redux/middlewares/MiddlewareRegistry';
import { User } from '../../base/types';
import { setUser, LOGIN, INIT_AUTH, setError, LOGOUT } from './authReducer';

const authMiddleware: Middleware = (middlewareAPI) => (next) => (action: Action) => {
    next(action);
    const handler = handlers[action.type];
    if (handler) {
        handler(middlewareAPI, action);
    }
};

export const BROWSER_STORAGE_AUTH_KEY = 'twitter-auth';

const handlers: Handlers<void> = {
    [LOGIN]: ({ dispatch }, action) => {
        axios
            .get(`/api/user?email=${action.payload}`)
            .then((response: AxiosResponse<User | null>) => {
                const { id, name, email } = response.data;
                const user: User = {
                    id,
                    name,
                    email,
                };
                dispatch(setUser(user));
                localStorage.setItem(BROWSER_STORAGE_AUTH_KEY, JSON.stringify(user));
            })
            .catch((err) => {
                if (err.response.status === 404) {
                    dispatch(setError('User does not exist'));
                    return;
                }
            });
    },
    [INIT_AUTH]: ({ dispatch }) => {
        const browserStorageAuthString = localStorage.getItem(BROWSER_STORAGE_AUTH_KEY);
        if (browserStorageAuthString) {
            const browserStorageAuth = JSON.parse(browserStorageAuthString);
            if (browserStorageAuth && browserStorageAuth.id && browserStorageAuth.id && browserStorageAuth.email) {
                dispatch(setUser(browserStorageAuth));
            }
        }
    },
    [LOGOUT]: () => {
        localStorage.removeItem(BROWSER_STORAGE_AUTH_KEY);
    },
};

MiddlewareRegistry.register(authMiddleware);

export default authMiddleware;
