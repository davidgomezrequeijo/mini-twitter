import { Middleware } from 'redux';
import axios, { AxiosResponse } from 'axios';
import { Action, Handlers } from '../../base/types/redux';
import MiddlewareRegistry from '../../base/redux/middlewares/MiddlewareRegistry';
import { SEARCH_USERS, setUsers } from './searchReducer';
import { User } from '../../base/types';

const searchMiddleware: Middleware = (middlewareAPI) => (next) => (action: Action) => {
    next(action);
    const handler = handlers[action.type];
    if (handler) {
        handler(middlewareAPI, action);
    }
};

const handlers: Handlers<void> = {
    [SEARCH_USERS]: async ({ dispatch }, action) => {
        axios.get(`/api/user/search?searchquery=${action.payload}`).then((response: AxiosResponse<User[]>) => {
            dispatch(setUsers(response.data));
        });
    },
};

MiddlewareRegistry.register(searchMiddleware);

export default searchMiddleware;
