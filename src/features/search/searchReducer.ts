import { Action } from '../../base/types/redux';
import ReducerRegistry from '../../base/redux/reducers/ReducerRegistry';
import { User } from '../../base/types';

export interface SearchState {
    loading: boolean;
    users: User[];
}

export const initialState: SearchState = {
    loading: false,
    users: [],
};

const searchReducer = (state: SearchState = initialState, action: Action<void | string | User[]>): SearchState => {
    switch (action.type) {
        case SEARCH_USERS:
            return {
                ...state,
                loading: true,
            };
        case SET_USERS:
            return {
                ...state,
                loading: false,
                users: action.payload as User[],
            };
        default:
            return state;
    }
};

export const SEARCH_USERS = 'search/searchUsers';
export const SET_USERS = 'search/setUsers';

export const searchUsers = (payload: string): Action<string> => ({
    type: SEARCH_USERS,
    payload,
});

export const setUsers = (payload: User[]): Action<User[]> => ({
    type: SET_USERS,
    payload,
});

ReducerRegistry.register('search', searchReducer);

export default searchReducer;
