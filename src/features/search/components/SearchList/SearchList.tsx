import React, { ReactElement } from 'react';
import { User } from '../../../../base/types';
import { StyledList } from '../../../../base/components/list';
import styled from 'styled-components';
import FollowingItem from './FollowingItem';

interface SearchListProps {
    users: User[];
    onClickFollow: (user: User) => void;
}

const StyledListContainer = styled.div`
    overflow-y: auto;
`;

const SearchList = ({ users, onClickFollow }: SearchListProps): ReactElement => {
    return (
        <StyledListContainer>
            <StyledList>
                {users.map((user) => (
                    <FollowingItem key={user.id} user={user} onClickFollow={onClickFollow} />
                ))}
            </StyledList>
        </StyledListContainer>
    );
};

export default SearchList;
