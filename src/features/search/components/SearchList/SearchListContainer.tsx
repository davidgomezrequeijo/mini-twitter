import React, { ReactElement } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState, User } from '../../../../base/types';
import SearchList from './SearchList';
import { followUser } from '../../../following/followingReducer';
import Loading from '../../../../base/components/loading';

const SearchListContainer = (): ReactElement => {
    const dispatch = useDispatch();
    const { loading, users } = useSelector((state: RootState) => state.search);

    const handleClickFollow = (user: User) => {
        dispatch(followUser(user));
    };

    if (loading) {
        return <Loading />;
    }

    return <SearchList users={users} onClickFollow={handleClickFollow} />;
};

export default SearchListContainer;
