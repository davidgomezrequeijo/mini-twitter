import React, { ReactElement } from 'react';
import { useDispatch } from 'react-redux';
import SearchInput from './SearchInput';
import { searchUsers, setUsers } from '../../searchReducer';

const SearchInputContainer = (): ReactElement => {
    const dispatch = useDispatch();

    const handleSearch = (userQuery: string) => {
        if (userQuery.length === 0) {
            dispatch(setUsers([]));
            return;
        }
        dispatch(searchUsers(userQuery));
    };

    return <SearchInput onSearch={handleSearch} />;
};

export default SearchInputContainer;
