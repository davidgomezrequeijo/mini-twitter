import React, { ReactElement } from 'react';
import { StyledInput } from '../../../../base/components/inputText/inputText';
import styled from 'styled-components';

interface SearchInputProps {
    onSearch: (message: string) => void;
}

const StyledInputSearch = styled(StyledInput)`
    box-sizing: border-box;
`;

const SearchInput = ({ onSearch }: SearchInputProps): ReactElement => {
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        onSearch(event.target.value);
    };

    return <StyledInputSearch type="text" placeholder="Search" onChange={handleChange} />;
};

export default SearchInput;
