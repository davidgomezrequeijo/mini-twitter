import React, { ReactElement } from 'react';
import SearchListContainer from './SearchList/SearchListContainer';
import SearchInputContainer from './SearchInput/SearchInputContainer';

const SearchContainer = (): ReactElement => {
    return (
        <>
            <SearchInputContainer />
            <SearchListContainer />
        </>
    );
};

export default SearchContainer;
