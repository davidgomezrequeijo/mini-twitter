import React, { ReactElement, useState } from 'react';
import { StyledInput } from '../../../../base/components/inputText/inputText';
import styled from 'styled-components';
import { StyledButton } from '../../../../base/components/button';

interface ShareboxProps {
    onShare: (message: string) => void;
}

const StyledInputShare = styled(StyledInput)`
    box-sizing: border-box;
`;

const StyledButtonShare = styled(StyledButton)`
    margin-top: 10px;
`;

const Sharebox = ({ onShare }: ShareboxProps): ReactElement => {
    const [message, setMessage] = useState<string>('');

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.value) {
            setMessage(event.target.value);
        }
    };

    const handleSendMessage = () => {
        if (message) {
            onShare(message);
        }
    };

    return (
        <>
            <StyledInputShare type="text" placeholder="Share something with your followers" onChange={handleChange} />
            <StyledButtonShare onClick={handleSendMessage}>Share</StyledButtonShare>
        </>
    );
};

export default Sharebox;
