import React, { ReactElement } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../../base/types';
import Sharebox from './Sharebox';
import { sendNewPost } from '../../shareReducer';
import Loading from '../../../../base/components/loading';

const ShareboxContainer = (): ReactElement => {
    const dispatch = useDispatch();
    const { loading } = useSelector((state: RootState) => state.share);

    const handleShare = (newMessage: string) => {
        dispatch(sendNewPost(newMessage));
    };

    if (loading) {
        return <Loading />;
    }

    return <Sharebox onShare={handleShare} />;
};

export default ShareboxContainer;
