import { Middleware } from 'redux';
import axios from 'axios';
import { Action, Handlers } from '../../base/types/redux';
import MiddlewareRegistry from '../../base/redux/middlewares/MiddlewareRegistry';
import { SEND_NEW_POST, messageSent } from './shareReducer';

const shareMiddleware: Middleware = (middlewareAPI) => (next) => (action: Action) => {
    next(action);
    const handler = handlers[action.type];
    if (handler) {
        handler(middlewareAPI, action);
    }
};

const handlers: Handlers<void> = {
    [SEND_NEW_POST]: async ({ getState, dispatch }, action) => {
        const currentUserId = getState().auth.user.id;
        axios.post(`/api/user/share/${currentUserId}`, { message: action.payload }).then(() => {
            dispatch(messageSent());
        });
    },
};

MiddlewareRegistry.register(shareMiddleware);

export default shareMiddleware;
