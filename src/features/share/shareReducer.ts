import { Action } from '../../base/types/redux';
import ReducerRegistry from '../../base/redux/reducers/ReducerRegistry';

export interface ShareState {
    loading: boolean;
}

export const initialState: ShareState = {
    loading: false,
};

const shareReducer = (state: ShareState = initialState, action: Action<void | string>): ShareState => {
    switch (action.type) {
        case SEND_NEW_POST:
            return {
                ...state,
                loading: true,
            };
        case MESSAGE_SENT:
            return {
                ...state,
                loading: false,
            };
        default:
            return state;
    }
};

export const SEND_NEW_POST = 'share/sendNewPost';
export const MESSAGE_SENT = 'share/messageSent';

export const sendNewPost = (payload: string): Action<string> => ({
    type: SEND_NEW_POST,
    payload,
});

export const messageSent = (): Action => ({
    type: MESSAGE_SENT,
});

ReducerRegistry.register('share', shareReducer);

export default shareReducer;
