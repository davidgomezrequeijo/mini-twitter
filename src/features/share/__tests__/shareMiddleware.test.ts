import { AnyAction, Dispatch } from 'redux';
import axios from 'axios';
import { sendNewPost } from '../shareReducer';
import shareMiddleware from '../shareMiddleware';

jest.mock('axios');

let dispatch: Dispatch<AnyAction>;
let getState: any;
let next: Dispatch<AnyAction>;

const setup = () => {
    dispatch = jest.fn();
    getState = jest.fn();
    getState.mockReturnValue({});
    next = jest.fn();
    jest.clearAllMocks();
};

describe('Following middleware', () => {
    beforeEach(() => {
        setup();
    });
    it('should post the new message to the API when sendNewPost is dispatched', () => {
        const userId = 123456;
        getState.mockReturnValue({
            auth: {
                user: {
                    id: userId,
                },
            },
        });
        const newPost = 'Test post';
        const action = sendNewPost(newPost);

        shareMiddleware({ dispatch, getState })(next)(action);

        expect(axios.post).toHaveBeenCalledWith(`/api/user/share/${userId}`, { message: newPost });
    });
});
