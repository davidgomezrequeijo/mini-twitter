import shareReducer, { sendNewPost, initialState, messageSent } from '../shareReducer';

describe('Share reducer', () => {
    let getState;
    const setup = () => {
        getState = jest.fn();
        getState.mockReturnValue({});
    };
    beforeEach(() => {
        setup();
    });
    it('It should set loading true, when dispatching sendNewPost', () => {
        const action = sendNewPost('New Post');

        expect(shareReducer(initialState, action)).toEqual({
            ...initialState,
            loading: true,
        });
    });
    it('It should set loading false, when dispatching messageSent', () => {
        const action = messageSent();

        expect(shareReducer(initialState, action)).toEqual({
            ...initialState,
            loading: false,
        });
    });
});
