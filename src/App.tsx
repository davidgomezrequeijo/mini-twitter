import React, { ReactElement } from 'react';
import configureStore from './base/redux/store';
import { Provider } from 'react-redux';
import Router from './base/router';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
    html {
        height: 100%
    }
    body {
        background-color: #15202b;
        margin: 0;
        height: 100%;
    }
    #root {
        height: 100%;
    }
`;

export default function App(): ReactElement {
    const store = configureStore();

    return (
        <>
            <GlobalStyle />
            <Provider store={store}>
                <Router />
            </Provider>
        </>
    );
}
