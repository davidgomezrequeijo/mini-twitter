import React, { ReactElement } from 'react';
import styled from 'styled-components';
import Layout from '../../base/components/layout';
import Sidebar from '../../base/components/layout/sidebar';
import Content from '../../base/components/layout/content';
import FollowingContainer from '../../features/following/components/FollowingUser';
import { PostListUserContainer } from '../../features/wall/components/PostList';
import { useParams } from 'react-router-dom';

const StyledContent = styled.div`
    height: 100%;
`;

const StyledFollowingContainer = styled.div`
    margin: 30px 30px 15px 30px;
`;

const User = (): ReactElement => {
    const { id } = useParams<{ id: string }>();

    return (
        <Layout>
            <Sidebar>
                <div>
                    <StyledFollowingContainer>
                        <FollowingContainer idUser={Number(id)} />
                    </StyledFollowingContainer>
                </div>
            </Sidebar>
            <Content>
                <StyledContent>
                    <PostListUserContainer idUser={Number(id)} />
                </StyledContent>
            </Content>
        </Layout>
    );
};

export default User;
