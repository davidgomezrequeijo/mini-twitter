import React, { ReactElement } from 'react';
import Register from '../../features/register/Register';

const RegisterPage = (): ReactElement => {
    return <Register />;
};

export default RegisterPage;
