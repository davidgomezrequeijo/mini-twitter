import React, { ReactElement } from 'react';
import Layout from '../../base/components/layout';
import Sidebar from '../../base/components/layout/sidebar';
import Content from '../../base/components/layout/content';
import FollowingContainer from '../../features/following/components/FollowingCurrentUser';
import { PostListWallContainer } from '../../features/wall/components/PostList';
import ShareboxContainer from '../../features/share/components/Sharebox/ShareboxContainer';
import SearchContainer from '../../features/search/components/SearchContainer';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { RootState } from '../../base/types';
import LogoutContainer from '../../features/auth/components/Logout';

const StyledSidebarHome = styled.div`
    display: grid;
    grid-template-rows: 60% 40%;
    height: 100%;
`;

const StyledContentHome = styled.div`
    display: grid;
    grid-template-rows: 20% 80%;
    height: 100vh;
`;

const StyledFollowingContainer = styled.div`
    margin: 30px 30px 15px 30px;
`;

const StyledSearchContainer = styled.div`
    margin: 15px 30px 30px 30px;
    display: flex;
    flex-direction: column;
`;

const StyledPostListWallContainer = styled.div`
    overflow-y: auto;
    border-top: 1px solid #808080;
`;

const StyledShareboxContainer = styled.div`
    margin: 50px;
`;

const Home = (): ReactElement => {
    const { user } = useSelector((state: RootState) => state.auth);
    return (
        <Layout>
            <Sidebar>
                <StyledSidebarHome>
                    <StyledFollowingContainer>
                        <LogoutContainer />
                        <FollowingContainer idUser={user.id} />
                    </StyledFollowingContainer>
                    <StyledSearchContainer>
                        <SearchContainer />
                    </StyledSearchContainer>
                </StyledSidebarHome>
            </Sidebar>
            <Content>
                <StyledContentHome>
                    <StyledShareboxContainer>
                        <ShareboxContainer />
                    </StyledShareboxContainer>
                    <StyledPostListWallContainer>
                        <PostListWallContainer />
                    </StyledPostListWallContainer>
                </StyledContentHome>
            </Content>
        </Layout>
    );
};

export default Home;
