import React, { ReactElement } from 'react';
import Login from '../../features/auth/components/Login';

const LoginPage = (): ReactElement => {
    return <Login />;
};

export default LoginPage;
