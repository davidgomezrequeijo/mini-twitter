import React, { ReactElement } from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RootState } from '../types';

type PrivateRouteProps = RouteProps & {
    children: React.ReactNode;
};

const PrivateRoute = ({ children, ...rest }: PrivateRouteProps): ReactElement => {
    const { user } = useSelector((state: RootState) => state.auth);
    return (
        <Route
            {...rest}
            render={({ location }) =>
                user !== null ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: location },
                        }}
                    />
                )
            }
        />
    );
};

export default PrivateRoute;
