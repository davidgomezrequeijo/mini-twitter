import React, { ReactElement, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from '../../pages/Home';
import User from '../../pages/User';
import NotFound from '../../pages/NotFound';
import Register from '../../pages/Register';
import Login from '../../pages/Login';
import PrivateRoute from './PrivateRoute';
import { useDispatch } from 'react-redux';
import { initAuth } from '../../features/auth/authReducer';

const RouterApp = (): ReactElement => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(initAuth());
    }, []);

    return (
        <Router>
            <Switch>
                <Route path="/login">
                    <Login />
                </Route>
                <Route path="/register">
                    <Register />
                </Route>
                <PrivateRoute path="/user/:id">
                    <User />
                </PrivateRoute>
                <PrivateRoute exact path="/">
                    <Home />
                </PrivateRoute>
                <Route path="*">
                    <NotFound />
                </Route>
            </Switch>
        </Router>
    );
};

export default RouterApp;
