import { FollowingState } from '../../features/following/followingReducer';
import { WallState } from '../../features/wall/wallReducer';
import { ShareState } from '../../features/share/shareReducer';
import { SearchState } from '../../features/search/searchReducer';
import { RegisterState } from '../../features/register/registerReducer';
import { AuthState } from '../../features/auth/authReducer';

export interface RootState {
    following: FollowingState;
    wall: WallState;
    share: ShareState;
    search: SearchState;
    register: RegisterState;
    auth: AuthState;
}

export interface User {
    id: number;
    name: string;
    email: string;
}

export interface Post {
    id: number;
    message: string;
    createdAt: string;
    user: User;
}
