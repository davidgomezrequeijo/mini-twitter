import { MiddlewareAPI, Dispatch } from 'redux';
import { RootState } from '.';

export interface Action<T = void> {
    type: string;
    payload?: T;
}

export interface Handlers<T> {
    [key: string]: (middlewareAPI: MiddlewareAPI<Dispatch, RootState>, action?: Action<T>) => void;
}
