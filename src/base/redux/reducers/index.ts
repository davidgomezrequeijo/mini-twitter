import '../../../features/following/followingReducer';
import '../../../features/wall/wallReducer';
import '../../../features/share/shareReducer';
import '../../../features/search/searchReducer';
import '../../../features/register/registerReducer';
import '../../../features/auth/authReducer';
