import { combineReducers, Action, AnyAction } from 'redux';
import type { Reducer } from 'redux';

declare type NameReducerMap<S = unknown, A extends Action = AnyAction> = {
    [name: string]: Reducer<S, A>;
};

class ReducerRegistry {
    _elements: NameReducerMap<unknown, Action>;

    constructor() {
        this._elements = {};
    }

    combineReducers(additional: NameReducerMap<unknown, Action> = {}) {
        return combineReducers({
            ...this._elements,
            ...additional,
        });
    }

    register(name: string, reducer: Reducer<any, AnyAction>) {
        this._elements[name] = reducer;
    }
}

export default new ReducerRegistry();
