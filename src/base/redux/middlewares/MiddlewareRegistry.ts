import { applyMiddleware, StoreEnhancer, Dispatch, AnyAction } from 'redux';
import type { Middleware } from 'redux';
import { RootState } from '../../types';

class MiddlewareRegistry {
    _middlewares: Array<Middleware<unknown, RootState, Dispatch<AnyAction>>>;

    constructor() {
        this._middlewares = [];
    }

    applyMiddleware(
        ...additional: Array<Middleware<unknown, RootState, Dispatch<AnyAction>>>
    ): StoreEnhancer<{ dispatch: () => void }> {
        return applyMiddleware(...this._middlewares, ...additional);
    }

    register(middleware: Middleware<unknown, RootState, Dispatch<AnyAction>>) {
        this._middlewares.push(middleware);
    }
}

export default new MiddlewareRegistry();
