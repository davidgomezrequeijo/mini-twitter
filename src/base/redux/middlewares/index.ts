import '../../../features/following/followingMiddleware';
import '../../../features/wall/wallMiddleware';
import '../../../features/share/shareMiddleware';
import '../../../features/search/searchMiddleware';
import '../../../features/register/registerMiddleware';
import '../../../features/auth/authMiddleware';
