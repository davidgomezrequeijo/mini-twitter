import { compose, createStore, Store } from 'redux';
import thunk from 'redux-thunk';
import '../middlewares';
import '../reducers';
import MiddlewareRegistry from '../middlewares/MiddlewareRegistry';
import ReducerRegistry from '../reducers/ReducerRegistry';

const middleware = MiddlewareRegistry.applyMiddleware(thunk);
const reducer = ReducerRegistry.combineReducers();

let composeEnhancers = compose;

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: typeof composeEnhancers;
    }
}

if (process.env.NODE_ENV === 'development') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const configureStore = (): Store => {
    return createStore(reducer, {}, composeEnhancers(middleware));
};

export default configureStore;
