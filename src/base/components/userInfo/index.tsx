import React, { ReactElement } from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { User } from '../../types';

const StyledNavLink = styled(NavLink)`
    color: #ffffff;
    display: flex;
    align-items: center;
    width: min-content;
`;

interface UserInfoProps {
    user: User;
}

const UserInfo = ({ user }: UserInfoProps): ReactElement => {
    return (
        <StyledNavLink to={`/user/${user.id}`}>
            <AccountCircleIcon fontSize="large" />
            <span>{user.name}</span>
        </StyledNavLink>
    );
};

export default UserInfo;
