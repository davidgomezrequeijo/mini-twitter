import styled from 'styled-components';

export const StyledTitle = styled.h2`
    font-family: 'Roboto', sans-serif;
    color: #ffffff;
`;
