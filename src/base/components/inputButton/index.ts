import styled from 'styled-components';

export const StyledInputButton = styled.input`
    background-color: #007ec3;
    border-radius: 4px;
    color: #ffffff;
    font-family: 'Roboto';
    outline: none;
    border: none;
    margin-top: 10px;
    padding: 10px;
    cursor: pointer;
`;
