import styled from 'styled-components';

export const StyledButton = styled.button`
    background-color: #007ec3;
    border-radius: 4px;
    color: #ffffff;
    font-family: 'Roboto';
    outline: none;
    border: none;
    padding: 7px;
    cursor: pointer;
`;
