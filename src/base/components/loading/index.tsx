import React, { ReactElement } from 'react';
import styled from 'styled-components';

const StyledLoading = styled.div`
    border: 5px solid #f3f3f3;
    border-radius: 50%;
    border-top: 5px solid #3498db;
    width: 30px;
    height: 30px;
    margin: 5px;
    animation: spin 2s linear infinite;

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
`;

const Loading = (): ReactElement => {
    return <StyledLoading />;
};

export default Loading;
