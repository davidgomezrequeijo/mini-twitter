import styled from 'styled-components';

export const StyledList = styled.ul`
    margin: 0px;
    list-style-type: none;
    padding: 0px;
`;
