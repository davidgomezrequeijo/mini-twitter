import React, { ReactElement } from 'react';
import styled from 'styled-components';

interface SidebarProps {
    children: React.ReactNode;
}

const StyledSidebar = styled.div`
    border-right: 1px solid #38444d;
`;

const Sidebar = ({ children }: SidebarProps): ReactElement => {
    return <StyledSidebar>{children}</StyledSidebar>;
};

export default Sidebar;
