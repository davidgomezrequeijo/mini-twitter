import React, { ReactElement } from 'react';
import styled from 'styled-components';

interface ContentProps {
    children: React.ReactNode;
}

const StyledContent = styled.div``;

const Content = ({ children }: ContentProps): ReactElement => {
    return <StyledContent>{children}</StyledContent>;
};

export default Content;
