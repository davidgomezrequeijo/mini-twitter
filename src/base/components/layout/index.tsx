import React, { ReactElement } from 'react';
import styled from 'styled-components';

interface LayoutProps {
    children: React.ReactNode;
}

const StyledLayout = styled.div`
    display: grid;
    grid-template-columns: 20% 1fr;
    height: 100%;
`;

const Layout = ({ children }: LayoutProps): ReactElement => {
    return <StyledLayout>{children}</StyledLayout>;
};

export default Layout;
