import styled from 'styled-components';

export const StyledInput = styled.input`
    border-radius: 4px;
    border: 1px solid #007ec3;
    background-color: #131c29;
    font-family: 'Roboto';
    caret-color: #ffffff;
    color: #ffffff;
    width: 100%;
    padding: 10px;

    &:focus {
        outline: none;
    }
`;
