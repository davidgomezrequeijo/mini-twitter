const users = [
    {
        id: 1,
        name: 'Alice',
    },
    {
        id: 2,
        name: 'Bob',
    },
    {
        id: 3,
        name: 'Charlie',
    },
];

const messages = [
    {
        id: 1,
        message: 'Hi!',
        user: {
            id: 1,
            name: 'Alice',
        },
    },
    {
        id: 2,
        message: 'Welcome!',
        user: {
            id: 2,
            name: 'Bob',
        },
    },
    {
        id: 3,
        message: 'How are you?',
        user: {
            id: 3,
            name: 'Charlie',
        },
    },
];

module.exports = {
    users,
    messages,
};
