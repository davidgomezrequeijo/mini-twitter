/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const mocks = require('./mocks');

const PORT = process.env.PORT || 3001;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

mongoose.Promise = global.Promise;
mongoose
    .connect('mongodb+srv://david:zpps9oJs9CxkbBeJ@cluster0.hkwth.mongodb.net/twitter?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        console.log('Connected To Mongo Db DataBase');
    })
    .catch((err) => {
        console.log('DataBase Connection Error ' + err);
    });

app.use(require('./routes'));

app.use(express.static(path.join(__dirname, '/../dist')));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/../dist/index.html'));
});

app.listen(PORT, () => console.log('Express server is running on localhost:', PORT));
