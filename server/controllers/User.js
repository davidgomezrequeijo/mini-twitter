// eslint-disable-next-line @typescript-eslint/no-var-requires
const User = require('../models/User');

module.exports = {
    create: async (req, res) => {
        const { id, name, email } = req.body.user;
        const user = await User.create({
            id,
            name,
            email,
        });

        return res.send(user);
    },

    find: async (req, res) => {
        const { email } = req.query;
        const user = await User.findOne({ email });
        if (!user) {
            return res.status(404).send('Not found');
        }
        return res.send(user);
    },
    findByProperty: async (req, res) => {
        const { searchquery } = req.query;
        const regex = new RegExp(searchquery, 'i');
        const user = await User.find({ email: regex });
        return res.send(user);
    },
    followUser: async (req, res) => {
        const { id } = req.body.currentUser;
        const idFollowUser = req.body.followUser.id;
        await User.update({ id: id }, { $push: { following: idFollowUser } });
        const followingIds = await User.findOne({ id }).select('following');
        const followingUsers = await User.find().where('id').in(followingIds.following);
        return res.send(followingUsers);
    },
    unfollowUser: async (req, res) => {
        const { id } = req.body.currentUser;
        const idUnfollowUser = req.body.unfollowUser.id;
        await User.update({ id: id }, { $pull: { following: idUnfollowUser } });
        const followingIds = await User.findOne({ id }).select('following');
        const followingUsers = await User.find().where('id').in(followingIds.following);
        return res.send(followingUsers);
    },
    followingUsers: async (req, res) => {
        const { id } = req.query;
        const followingIds = await User.findOne({ id }).select('following');
        let followingUsers = [];
        if (followingIds) {
            followingUsers = await User.find().where('id').in(followingIds.following);
        }
        return res.send(followingUsers);
    },
};
