/* eslint-disable @typescript-eslint/no-var-requires */
const Post = require('../models/Post');
const User = require('../models/User');

module.exports = {
    create: async (req, res) => {
        const { id } = req.params;
        const { message } = req.body;
        const userById = await User.findOne({ id });
        const post = await Post.create({
            id: new Date().getTime(),
            message,
            user: userById,
        });
        await User.update({ id: id }, { $push: { posts: post } });
        await userById.save();

        return res.send(userById);
    },
    allPosts: async (req, res) => {
        const { id } = req.params;
        const followingUserIds = await User.findOne({ id }).select('following');
        const allUserIds = [...followingUserIds.following, id];
        const allPosts = await Post.find().populate('user').sort({ createdAt: 'desc' });
        const filteredPosts = allPosts.filter((post) => allUserIds.includes(post.user.id));
        return res.send(filteredPosts);
    },
    postsUser: async (req, res) => {
        const { id } = req.params;
        const allPosts = await Post.find().populate('user').sort({ createdAt: 'desc' });
        const filteredPosts = allPosts.filter((post) => id === post.user.id);
        return res.send(filteredPosts);
    },
};
