/* eslint-disable @typescript-eslint/no-var-requires */
const express = require('express');
const User = require('./controllers/User');
const Post = require('./controllers/Post');

const router = new express.Router();

router.post('/api/user/register', User.create);
router.get('/api/user', User.find);
router.get('/api/user/search', User.findByProperty);
router.post('/api/user/follow', User.followUser);
router.post('/api/user/unfollow', User.unfollowUser);
router.get('/api/user/following', User.followingUsers);

router.post('/api/user/share/:id', Post.create);
router.get('/api/user/allposts/:id', Post.allPosts);
router.get('/api/user/posts/:id', Post.postsUser);

module.exports = router;
