// eslint-disable-next-line @typescript-eslint/no-var-requires
const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema(
    {
        id: {
            type: String,
            required: '{PATH} is required!',
        },
        message: {
            type: String,
            required: '{PATH} is required!',
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
    },
    {
        timestamps: true,
    },
);

module.exports = mongoose.model('Post', PostSchema);
