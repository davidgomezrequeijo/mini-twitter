// eslint-disable-next-line @typescript-eslint/no-var-requires
const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
    {
        id: {
            type: String,
            required: '{PATH} is required!',
        },
        name: {
            type: String,
            required: '{PATH} is required!',
        },
        email: {
            type: String,
            required: '{PATH} is required!',
        },
        posts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Post' }],
        following: [{ type: String }],
    },
    {
        timestamps: true,
    },
);

module.exports = mongoose.model('User', UserSchema);
