const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, argv) => {
    const isProduction = argv.mode === 'production';

    const config = {
        entry: {
            index: path.join(__dirname, 'src', 'index.tsx'),
        },
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: isProduction ? '[name].[chunkhash].js' : '[name].js',
            publicPath: '/',
        },
        mode: argv.mode,
        target: 'web',
        resolve: {
            extensions: ['.ts', '.tsx', '.js'],
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'ts-loader',
                    exclude: '/node_modules/',
                },
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: path.join(__dirname, 'templates', 'index.html'),
            }),
        ],
    };
    if (!isProduction) {
        config.devtool = 'source-map';
        config.devServer = {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 8000,
            disableHostCheck: true,
            historyApiFallback: true,
            proxy: { '/api/**': { target: 'http://localhost:3001', secure: false } },
        };
    }
    return config;
};
